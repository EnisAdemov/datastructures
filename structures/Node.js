var DataStructures;
(function (DataStructures) {
    var Node = (function () {
        function Node(value) {
            this.value = value;
        }
        return Node;
    })();
    DataStructures.Node = Node;
})(DataStructures || (DataStructures = {}));
//# sourceMappingURL=Node.js.map