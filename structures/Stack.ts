///<reference path="Node.ts"/>

module DataStructures {
    export class Stack<T> {
        top: Node<T>;
        elementToReturn: T;
        newNode: Node<T>;

        constructor() {
            this.top = null;
        }

        pop() {
             this.elementToReturn = this.top.value;
             this.top = this.top.next;
             return this.elementToReturn;
        }

        push(value:T) {
            this.newNode = new Node(value);
            this.newNode.next = this.top;
            this.top = this.newNode;
        }

        toString() {
            let result = "";
            while(this.top != null) {
                result += this.pop() + "   ";
            }
            return result;
        }
    }
}