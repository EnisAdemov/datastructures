///<reference path="structures/DoubleLinkedList.ts"/>
///<reference path="structures/Vertex.ts"/>
///<reference path="structures/Stack.ts"/>
///<reference path="structures/Tree.ts"/>

module DataStructures {
    class DataStructures {
        DoubleLinkedList: DoubleLinkedList<number>;
        Stack: Stack<number>;
        Tree: Tree<number>;

        constructor() {
            this.DoubleLinkedList = new DoubleLinkedList<number>();
            this.Stack = new Stack<number>();
            this.Tree = new Tree<number>(1);

            //fill the DoublyLinkedList
            this.DoubleLinkedList.addFirst(55);
            this.DoubleLinkedList.addFirst(99);
            this.DoubleLinkedList.addLast(22);
            this.DoubleLinkedList.addLast(11);

            //fill the Stack
            this.Stack.push(777);
            this.Stack.push(666);
            this.Stack.push(555);

            //fill the Tree
            this.Tree.root.addVertexes(4, 2, 6);
            this.Tree.root.addVertexes(7, 3);

            let result = document.getElementById('item1');
            result.innerHTML += this.DoubleLinkedList.toString();

            result = document.getElementById('item2');
            result.innerHTML += this.Stack.toString();

            result = document.getElementById('item3');
            result.innerHTML += this.Tree.toString();
        }

    }
    window.onload = () => {
        new DataStructures();
    };
}