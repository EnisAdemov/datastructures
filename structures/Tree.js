///<reference path="Vertex.ts"/>
///<reference path="Stack.ts"/>
var DataStructures;
(function (DataStructures) {
    var Tree = (function () {
        function Tree(value) {
            this.root = new DataStructures.Vertex(value, 0);
        }
        Tree.prototype.toString = function () {
            var _this = this;
            let result = "";
            this.searchStack = new DataStructures.Stack();
            this.searchStack.push(this.root);
            while (this.searchStack.top != null) {
                this.currentVertex = this.searchStack.pop();
                result += ' ' + this.currentVertex.depth * 1 + "-" + this.currentVertex.value;
                result += '\n';
                this.currentVertex.children.forEach(function (child) {
                    _this.searchStack.push(child);
                });
                this.currentVertex.children.reverse();
            }
            return result;
        };
        return Tree;
    })();
    DataStructures.Tree = Tree;
})(DataStructures || (DataStructures = {}));
//# sourceMappingURL=Tree.js.map