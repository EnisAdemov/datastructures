///<reference path="Node.ts"/>

module DataStructures {
    export class DoubleLinkedList<T> {
        count: number;
        firstNode: Node<T>;
        lastNode: Node<T>;
        newNode: Node<T>;
        tempNode: Node<T>;

        constructor() {
            this.count = 0;
            this.firstNode = null;
            this.lastNode = null;
         }

        addLast(value:T) {
            this.count++;
            this.newNode = new Node(value);

            if (this.firstNode == null) {
                this.firstNode = this.newNode;
                this.lastNode = this.firstNode;
                return;
            }

            this.lastNode.next = this.newNode;
            this.newNode.prev = this.lastNode;
            this.lastNode = this.newNode;
        }

        addFirst(value:T) {
            this.count++;
            this.newNode = new Node(value);

            this.newNode.next = this.firstNode;
            this.newNode.prev = null;
            this.firstNode = this.newNode;

            if (this.lastNode == null) {
                this.lastNode = this.firstNode;
                return;
            }
        }

        //FIX THIS LATER
        addBefore(index:number, value:T) {
            this.count++;
            this.newNode = new Node(value);
            this.tempNode = new Node(value);
            this.tempNode = this.firstNode;
            while(this.tempNode != this.getNode(index)) {
                console.log(this.tempNode.value);
                this.tempNode = this.tempNode.next;
            }
            this.newNode.next = this.tempNode.next;
            this.newNode.prev = this.tempNode;
            this.tempNode.next = this.newNode;
        }

        getNode(index:number) {
            let temp = 0;
            this.newNode = new Node(null);
            this.newNode = this.firstNode;
            while (temp < index ) {
                this.newNode = this.newNode.next;
                temp++;
            }
            return this.newNode;
        }

        toString() {
            let result = "";
            this.newNode = new Node(null);
            this.newNode = this.firstNode;
            while (this.newNode != null) {
                result += this.newNode.value + "   ";
                this.newNode = this.newNode.next;
            }
            return result;
        }
    }
}