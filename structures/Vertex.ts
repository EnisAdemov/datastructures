module DataStructures {
    export class Vertex<T> {
        value: T;
        parent: Vertex<T>;
        newChild: Vertex<T>;
        children: Array<Vertex<T>>; //not sure about this
        depth: number;

        constructor(value:T, depth:number) {
            this.value = value;
            this.depth = depth;
            this.children = new Array<Vertex<T>>();
        }

        addVertex(value:T) { //not good depth implemented
            this.newChild = new Vertex<T>(value, this.depth + 1);
            this.newChild.parent = this;
            this.children.push(this.newChild);
        }

        removeVertex(value: T) {
            this.children.forEach( (item, index) => {
                if(item.value === value) {
                    this.children.splice(index, 1);
                }
            });
        }

        addVertexes(...children: T[]) {
            children.forEach( (child) => {
                this.addVertex(child);
            });
        }
    }
}