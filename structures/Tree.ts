///<reference path="Vertex.ts"/>
///<reference path="Stack.ts"/>

module DataStructures {
    export class Tree<T> {
        root: Vertex<T>;
        searchStack: Stack<Vertex<T>>;
        currentVertex: Vertex<T>;

        constructor(value:T) {
            this.root = new Vertex<T>(value, 0);
        }

        toString() {
            let result = "";
            this.searchStack = new Stack<Vertex<T>>();
            this.searchStack.push(this.root);

            while (this.searchStack.top != null) {
                this.currentVertex = this.searchStack.pop();
                result += ' ' + this.currentVertex.depth*1  + "-" +  this.currentVertex.value;
                result += '\n';
                this.currentVertex.children.forEach( (child) => {
                    this.searchStack.push(child);
                });
                this.currentVertex.children.reverse();
            }
            return result;
        }
    }
}