///<reference path="Node.ts"/>
var DataStructures;
(function (DataStructures) {
    var Stack = (function () {
        function Stack() {
            this.top = null;
        }
        Stack.prototype.pop = function () {
            this.elementToReturn = this.top.value;
            this.top = this.top.next;
            return this.elementToReturn;
        };
        Stack.prototype.push = function (value) {
            this.newNode = new DataStructures.Node(value);
            this.newNode.next = this.top;
            this.top = this.newNode;
        };
        Stack.prototype.toString = function () {
            let result = "";
            while (this.top != null) {
                result += this.pop() + "   ";
            }
            return result;
        };
        return Stack;
    })();
    DataStructures.Stack = Stack;
})(DataStructures || (DataStructures = {}));
//# sourceMappingURL=Stack.js.map