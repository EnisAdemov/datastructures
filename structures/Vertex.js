var DataStructures;
(function (DataStructures) {
    var Vertex = (function () {
        function Vertex(value, depth) {
            this.value = value;
            this.depth = depth;
            this.children = new Array();
        }
        Vertex.prototype.addVertex = function (value) {
            this.newChild = new Vertex(value, this.depth + 1);
            this.newChild.parent = this;
            this.children.push(this.newChild);
        };
        Vertex.prototype.removeVertex = function (value) {
            var _this = this;
            this.children.forEach(function (item, index) {
                if (item.value === value) {
                    _this.children.splice(index, 1);
                }
            });
        };
        Vertex.prototype.addVertexes = function () {
            var _this = this;
            var children = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                children[_i - 0] = arguments[_i];
            }
            children.forEach(function (child) {
                _this.addVertex(child);
            });
        };
        return Vertex;
    })();
    DataStructures.Vertex = Vertex;
})(DataStructures || (DataStructures = {}));
//# sourceMappingURL=Vertex.js.map