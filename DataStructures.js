///<reference path="structures/DoubleLinkedList.ts"/>
///<reference path="structures/Vertex.ts"/>
///<reference path="structures/Stack.ts"/>
///<reference path="structures/Tree.ts"/>
var DataStructures;
(function (_DataStructures) {
    var DataStructures = (function () {
        function DataStructures() {
            this.DoubleLinkedList = new _DataStructures.DoubleLinkedList();
            this.Stack = new _DataStructures.Stack();
            this.Tree = new _DataStructures.Tree(1);
            //fill the DoublyLinkedList
            this.DoubleLinkedList.addFirst(55);
            this.DoubleLinkedList.addFirst(99);
            this.DoubleLinkedList.addLast(22);
            this.DoubleLinkedList.addLast(11);
            //fill the Stack
            this.Stack.push(777);
            this.Stack.push(666);
            this.Stack.push(555);
            //fill the Tree
            this.Tree.root.addVertexes(4, 2, 6);
            this.Tree.root.addVertexes(7, 3);
            let result = document.getElementById('item1');
            result.innerHTML += this.DoubleLinkedList.toString();
            result = document.getElementById('item2');
            result.innerHTML += this.Stack.toString();
            result = document.getElementById('item3');
            result.innerHTML += this.Tree.toString();
        }
        return DataStructures;
    })();
    window.onload = function () {
        new DataStructures();
    };
})(DataStructures || (DataStructures = {}));
//# sourceMappingURL=DataStructures.js.map