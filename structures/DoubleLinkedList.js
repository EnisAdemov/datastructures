///<reference path="Node.ts"/>
var DataStructures;
(function (DataStructures) {
    var DoubleLinkedList = (function () {
        function DoubleLinkedList() {
            this.count = 0;
            this.firstNode = null;
            this.lastNode = null;
        }
        DoubleLinkedList.prototype.addLast = function (value) {
            this.count++;
            this.newNode = new DataStructures.Node(value);
            if (this.firstNode == null) {
                this.firstNode = this.newNode;
                this.lastNode = this.firstNode;
                return;
            }
            this.lastNode.next = this.newNode;
            this.newNode.prev = this.lastNode;
            this.lastNode = this.newNode;
        };
        DoubleLinkedList.prototype.addFirst = function (value) {
            this.count++;
            this.newNode = new DataStructures.Node(value);
            this.newNode.next = this.firstNode;
            this.newNode.prev = null;
            this.firstNode = this.newNode;
            if (this.lastNode == null) {
                this.lastNode = this.firstNode;
                return;
            }
        };
        //FIX THIS LATER
        DoubleLinkedList.prototype.addBefore = function (index, value) {
            this.count++;
            this.newNode = new DataStructures.Node(value);
            this.tempNode = new DataStructures.Node(value);
            this.tempNode = this.firstNode;
            while (this.tempNode != this.getNode(index)) {
                console.log(this.tempNode.value);
                this.tempNode = this.tempNode.next;
            }
            this.newNode.next = this.tempNode.next;
            this.newNode.prev = this.tempNode;
            this.tempNode.next = this.newNode;
        };
        DoubleLinkedList.prototype.getNode = function (index) {
            let temp = 0;
            this.newNode = new DataStructures.Node(null);
            this.newNode = this.firstNode;
            while (temp < index) {
                this.newNode = this.newNode.next;
                temp++;
            }
            return this.newNode;
        };
        DoubleLinkedList.prototype.toString = function () {
            let result = "";
            this.newNode = new DataStructures.Node(null);
            this.newNode = this.firstNode;
            while (this.newNode != null) {
                result += this.newNode.value + "   ";
                this.newNode = this.newNode.next;
            }
            return result;
        };
        return DoubleLinkedList;
    })();
    DataStructures.DoubleLinkedList = DoubleLinkedList;
})(DataStructures || (DataStructures = {}));
//# sourceMappingURL=DoubleLinkedList.js.map